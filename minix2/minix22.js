let d=0

function setup(){
createCanvas(700,700);
}
function draw(){
background(255);

push();
stroke(0);
fill(255,0,0);
textSize(30);
text('-try pressing the mouse for a while', 60, 100);
pop();

push();
stroke(0);
fill(0,0,255);
textSize(30);
text('-this is where it all started', 300, 620);
pop();

//mund
fill(0);
ellipse(430,325,150,175);
fill(255);
ellipse(430,325,100,125);

//usynlig firkant
push();
noStroke();
fill(255);
rect(325,200,100,400);
pop();

//øjne og næse
fill(0);
square(200,250,50);
square(200,350,50);
rect(300,305,100,40);

if (mouseIsPressed){
    fill(255,255,0);
    ellipse(350, 325, d);
    d++
    
    }
 else{
    d=0
    }

if (d>400){
    d = 0;
    d = d + 1;
}

push();
if (d>300){
//mund
fill(0);
ellipse(350,375,175,150);
fill(255,255,0);
ellipse(350,370,125,100);

//usynlig firkant
push();
noStroke();
fill(255,255,0);
rect(225,295,250,75);
pop();

//øjne
fill(0);
circle(280,270,50);
circle(420,270,50);
             }               
pop();

}