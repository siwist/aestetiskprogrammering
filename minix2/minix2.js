

//Bevæglese af emojji med firkantede

let y1=0;
let y2=-80;
let y3=-80;
let y4=75;
let y5=-35;
let y6=-35;
let y7=-45;
let y8=-45;
let y9=-45;
let y10=-25;
let y11=-25;
let y12=-45;
let y13=45;
let y14=45;
let y15=50;
let y16=-35;
let y17=-35;
let y18=-40;
let y19=-40;


let h=90;
let b=90;
let d=50;
let d1=25;
let d2=5;
let d3=15;

let sort=0;
let gul=195;
let hvid=255;


function setup() {
  // Størrelsen på canvas
  createCanvas(900,windowHeight);


}

function draw(){
  // Grundbaggrund 
  background(51,153,153);
  strokeWeight(2);
  stroke(0);

fill(255,0,0);
textSize(30);
text('Try clicking', 60, 50);

  
  //emoji med firkantede øjne
  fill(255,215,102);
  ellipse(425,y1,400);
  y1++;
  //højre lukket øje
  line(465,y7,555,y7);
  y7++;
  //rynke under højre øje
  line(465,y9,535,y10);
  y9++;
  y10++;
  //lukket venstre øje
  line(300,y8,390,y8);
  y8++;
  //rynke under venstre øje
  line(320,y11,390,y12);
  y11++;
  y12++;
  //mund
  fill(255);
  rect(405,y15,50,50,50,PI);
  y15++
  fill(35);
  rect(405,y4,50,25);
  y4++;
  //øjne
  stroke(sort);
    fill(255,255,255);
  rect(465,y2,b,h);
  y2++;
  stroke(sort);
  rect(300,y3,b,h);
  y3++;
  fill(255,0,0);
  circle(345,y5,d);
  y5++;
  circle(510,y6,d);
  y6++;
  noStroke();
  fill(255,215,165);
  ellipse(315,y13,75,50);
  y13++;
  noStroke();
  fill(255,215,165);
  ellipse(545,y14,75,50);
  y14++;
  fill(sort);
  circle(345,y16,d1);
  y16++;
  circle(510,y17,d1);
  y17++;
  fill(hvid);
  ellipse(520,y18,d2,d3);
  ellipse(355,y19,d2,d3);
  y18++;
  y19++;



 if (y1>1000){
  y1=0
}
if (y2>1000){
  y2=0
}
if (y3>1000){
  y3=0
}
if (y4>1000){
  y4=0
}
if (y5>1000){
  y5=0
}
if (y6>1000){
  y6=0
}
if (y7>1000){
  y7=0
}
if (y8>1000){
  y8=0
}
if (y9>1000){
  y9=0
}
if (y10>1000){
  y10=0
}
if (y11>1000){
  y11=0
}
if (y12>1000){
  y12=0
}
if (y13>1000){
  y13=0
}
if (y14>1000){
  y14=0
}
if (y15>1000){
  y15=0
}
if (y16>1000){
  y16=0
}
if (y17>1000){
  y17=0
}
if (y18>1000){
  y18=0
}
if (y19>1000){
  y19=0
}

if (mouseIsPressed){
  h=0
  b=0
  d=0
  d1=0
  d2=0
  d3=0
  sort=gul
  hvid=gul
 }
  else{
    h=90
    b=90
    d=50
    d1=25
    d2=5
    d3=15
    sort=0
    hvid=255
  }

}
