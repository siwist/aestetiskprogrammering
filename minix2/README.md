# minix2
Her er et par screenshots af min første emoji:


<img src="minix2/Skærmbillede_20230219_201008.png">
<img src="minix2/Skærmbillede_20230219_201056.png">

Link til [emoji 1](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix2/index.html)


Her er et par screenshots af min andene emoji:


<img src="minix2/Skærmbillede_20230219_201111.png">
<img src="minix2/Skærmbillede_20230219_201123.png">

Her er et link til min anden emoji
Link til [emoji 2](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix2/index2.html)

# Beskrivelse af programmerne
Mit første program skal forestille en emoji der har kigget for meget på en skærm og er blevet træt i øjenne. Det gør den ved at den har firkantede røde øjne, og en måbende mund. Når man klikker med musen blinker emojien med øjnene, og emojien ser så træt ud når den har lukkede øjne. Emojien bevæger sig også hele tiden nedad på skærmen, og oppe i ventre hjørne står der at man kan prøve at klikke med musen, så man ved at det er en effekt i mit program.


Mit andet program viser først en simpel smiley med tegnene :-), hvilket skal illustrere hvordan vi startede med at lave smileys på de helt gamle telefoner der ikke havde emojis. Herefter, hvis man holder musen nede, dukker der en emoji op som vi kender den i dag - dog i en forsimplet udgave. Oppe i venstre hjørne står der skrevet at man kan prøve at holde musen inde et stykke tid, igen for at fortælle at der kan ske en form for interaktion. Nede i højre hjørne står der, at det var her det hele startede, hvilket henviser til hele denatten om emojis og de problematikker der dukkede op da emojis kom frem.

# Forklaring af koden til første emoji
Jeg startede med at sætte en masse y værdier for alle delene af emojien. Jeg har taget et eksempel fra min kode, men har ellers 19 y-værdier. Jeg tænker der må være en nemmere måde at få di forskellige figurer til at bevæge sig, men det kan jeg ikke finde ud af endnu.
~~~~//Bevæglese af emojji med firkantede
let y1=0;
let y2=-80;
let y3=-80;
let y4=75;'
~~~~
Derefter satte jeg nedenstående værdier, for at få øjene til at forsvinde når man klikke med musen.
~~~~
let h=90;
let b=90;
let d=50;
let d1=25;
let d2=5;
let d3=15;
~~~~
Jeg satte også nogle farveværdier, da der eller ville komme bitte små prikker der hvor øjene blev formindsket, da stroken var sort. Derfor tænkte jeg i forbindesle med aat øjenene blev små når man trykkede med musen, at stroken her kunne ændres til grå(den har jeg så kaldt gul), da det så ville blænde ind i emojien farve så man ikke ville kunne se prikkerne.
~~~~
let sort=0;
let gul=195;
let hvid=255;
~~~~
Efter jeg havde dannet mit lærred, satte jeg en sort stroke med en tykkelse på 2,og en rød tekst i størrelsen 30.
~~~~
  strokeWeight(2);
  stroke(0);
fill(255,0,0);
textSize(30);
text('Try clicking', 60, 50);
~~~~
Herefter lavede jeg emojien med lukkede øjne, hvor jeg ved hver figur, har tilføjet dens y-værdi. Nedenunder har jeg skrevet dens y-værdi igen med to plusser efter, der betyder at dens y-værdi konstant skal stige med en værdi. De skæve linjer der skal forestille rynkerne under de lukkede øjne, har jeg givet to forskellige y-værdier, da linjen ikke er vandret og dermed ikke kan bruge de samme y-værdier til linjens punkter til at bevæge sig med.
~~~~
  //højre lukket øje
  line(465,y7,555,y7);
  y7++;
  //rynke under højre øje
  line(465,y9,535,y10);
  y9++;
  y10++;
  ~~~~
Derefter lavede jeg de åbne øjne oven på de lukkede. Og satte farverne der skulle skifte til gul ind.
~~~~
  //pupiller
  fill(sort);
  circle(345,y16,d1);
  y16++;
  circle(510,y17,d1);
  y17++;
  fill(hvid);
  ellipse(520,y18,d2,d3);
  ellipse(355,y19,d2,d3);
  y18++;
  y19++;
  ~~~~
Så gjorde jeg så emojien ville komme op i toppen igen ved at gøre således med alle y-værdierne:
~~~~
 if (y1>1000){
  y1=0
  ~~~~
For at få emojien til at blinke brugte jeg mouseIsPressed funktionen, hvor at alle værdierne ville blive nul for de firkantede øjne hvis man trykkede med musen, og tilbage hvis ikke. Jeg tænker at jeg her kunne havde brugt en nemmere løsning også. Jeg  prøvede funktionen redraw(), men kunne ikke få det til at virke som jeg gerne ville have det.
~~~~
if (mouseIsPressed){
  h=0
  b=0
  d=0
  d1=0
  d2=0
  d3=0
  sort=gul
  hvid=gul
 }
  else{
    h=90
    b=90
    d=50
    d1=25
    d2=5
    d3=15
    sort=0
    hvid=255
  }
~~~~


Jeg synes jeg har lært at bruge de forskellige former til at danne et udtryk. For eksempel var det ikke meningen munden skulle se sådan ud til at starte med, men jeg synes den gav et godt udtryk da jeg kom til at lave den form. Jeg har eksperimenteret en masse med bevægelse og klik, men synes dog jeg fik en lidt lang kode, i forhold til hvordan jeg tænker det kunne have været.

# Forklaring af koden til anden emoji
Jeg startede med at sætte en diameter på 0.
~~~~
let d=0
~~~~
Efter jeg har sat mit lærred, skriver jeg de to sætniger. Jeg bruger funktionen push/pop() for at koden for hver sætning ikke bliver blandet sammen. Eksempel på den ene:
~~~~
push();
stroke(0);
fill(255,0,0);
textSize(30);
text('-try pressing the mouse for a while', 60, 100);
pop();
~~~~
Derefter laver jeg den simple smiley. Herunder ses koden for munden, som var sværere end øjnene og næsen, da jeg ikke kunne finde ud af at kode en stor parantes. Derfor løste jeg det ved at sætte en hvid cirkel oven på en sort, og så en firkant over halvdelen.
~~~~
//mund
fill(0);
ellipse(430,325,150,175);
fill(255);
ellipse(430,325,100,125);

//usynlig firkant
push();
noStroke();
fill(255);
rect(325,200,100,400);
pop();
~~~~
For at få den gule emoji til at komme frem brugte jeg igen funktionen mouseIsPressed(), hvor diameteren for ellipsen jo var sat til 0 fra start, og så ville vokse når man holder musen inde. Hvia man stopper med at holde musen inde eller at diameteren bliver stærre end 400, vil diameteren blive 0 igen.
~~~~
if (mouseIsPressed){
    fill(255,255,0);
    ellipse(350, 325, d);
    d++
    
    }
 else{
    d=0
    }

if (d>400){
    d = 0;
    d = d + 1;
}
~~~~
Jeg fik ansigtet på emojien til at komme frem ved at lave en if funktion, hvor at hvis diameteren blev større end 300, ville mund, øjne og næse komme frem. Herunder er eksempel for mund, hvor jeg har lavet samme trick med to cirkler og en firkant.
~~~~
push();
if (d>300){
//mund
fill(0);
ellipse(350,375,175,150);
fill(255,255,0);
ellipse(350,370,125,100);
~~~~
Denne kode er egentlig ret simpelt, men jeg synes det var intersant at jeg ville lave en figur jeg ikke vidste hvordan jeg skulle kode, og derfor måtte finde frem til en anden løsning.
# Tanker om emojis
Generelt kan jeg godt lide at bruge emojis. Jeg synes det kan gøre en besked fx. sød eller mere markeret hvis man tilføjer lidt af dem. Jeg vil ikke sige at jeg egentlig som sådan bruger så mange emojis generelt i hverdagen, hvis jeg nu tænker over det. Hvis jeg gør, er det mest hjerter eller en trendy emoji med hat på, fordi den ser fin ud og pynter lidt på en besked. Men det tror jeg mere handler om at det tager for lang tid at skulle vælge blandt de mange vi har, eller hvis jeg skal være ærlig, at det ikke er så trendy at bruge emojis hele tiden længere. Nu har det været her et stykke tid efterhånden, men der var en periode hvor vi alle (i hvert fald i min generation) begyndte at vende tilbage til de helt simple smileys som :-) eller hjertet med et krokodillenæb og et tretal. Især krokodillenæbs-hjertet er jeg storforbruger af, for det er fint og minder lidt om dengang man havde små Nokia-telefoner der ikke kunne så meget i 10'erne. 


Personligt ville jeg ikke mene at det decideret er en nødvendighed at have emojis, altså at vi ikke kan have et digitalt netværk uden dem. Dog er det forståeligt at de er kommet frem, da alle små forskellige funktioner på vores telefoner kan være sjove, og det kan også siges at emojis har været et hit og har haft en kæmpe indflydelse på vores hverdag på internettet. En vigtig ting at tage i mente er dog at man burde huske at det er et tilvalg, og ikke noget vi som sagt har brug for. Måske kan det hjælpe med at man ikke misforstår beskeder, men det kan ligeså vel ske ved brug af emojis også. 
Ved at have så mange emojis kan der også opstå problemer. Et problem der er interessant at kigge på er, Da vi efterhånden har så mange emojis at det læner sig op af, at kunne det samme som det japanske tegnsystem - begynder emojis nærmest at kunne sammenlignes med et sprog. Her er det nemlig ikke længere bare små tegninger, men noget der bliver politisk, etisk og generelt en meget stor ting med stor indflydelse. 


Et andet, mere omdiskuteret problem, er hele debatten der er opstået ift. køn, race, seksualitet osv..  Aldrig har jeg selv tænkt over de problemer ved mit eget forbrug. Dog er jeg også privilegeret som cis, hvid kvinde. For eksempel har stort set alle børnefilm, børnebøger og generelt mange ting i hverdagen,  indeholdt ting der repræsenterede mig. Derfor er det forståeligt, at der har været et opråb fra ikke-repræsenterede grupper.  "… but they also tend to oversimplify and universalize differences, thereby perpetuating normative ideologies within already “violent power structures,”  such that only selected people, those with specific skin tones for instance, are represented while others are not. There is a distinct inequality as to how people are represented, and we need to question who sets the standards for these representations." Som Soon Winnie & Cox skriver i kapitlet "Variable Geometry", handler det stadig ikke kun om små grupper der ikke er blevet repræsenteret, men også om hvilken type person de emojis der skal forskellige højtstillede autoriteter, så som læger og bussinesmænd, har fået. Dette bidrager til nogle allerede tilstedeværende magtbalancer der ses i vores samfund.


På trods af at jeg personligt synes debatten er stukket en smule af, og at vi har alt for mange emojis, forstår jeg godt og synes også det er vigtigt at debatten har været der. Hvis vi potentielt havde med et ægte nyt sprog at gøre, ville det så være i orden, hvis man ikke kunne fortælle om andre end dem der er mest privilegerede i samfundet?
Hele starten på denne debat er det min anden emoji repræsenterer, da det hele startede der hvor en smiley ikke længere blot var nogle tegn. Den første emoji repræsenterer når man har siddet foran en skærm og brugt så mange timer at man får firkantede øjne - den har vi jo ikke i vores emoji-sprog:-) 


Litteratur:

• Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70

