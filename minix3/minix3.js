let vinkel = 0;
let salto = 0;
let speed = 4;
let speed2 = 3.5;
let speed3 = 4;
let speed4 = 4;
let ben = -42;
let ben2 = -55;
let ben3 = 25;
let ben4 = 35;
let spin = -10;
let x = 20;
var dots = [" ",".","..","...","...."];
var index = 0;

function setup(){
    createCanvas(windowWidth,windowHeight);
background(255);
angleMode(DEGREES);
rectMode(CENTER);

}

function draw(){
    background(253);
    fill(255);
    rect(117.5, 117.5,35,35);

  //tekster
  push();
  textSize(15);
  stroke(0);
  strokeWeight(3);
  fill(255);

  textFont("Staatliches");
  text("- mouse here to give me a break <3",20,80);

  textFont("Staatliches");
  text("- hold the mouse to see me spinning",200,40);
  pop();

    translate(width/2, height/2);
    fill(220);
    ellipse(0,0,285);
    fill(214,163,112);

    //linjerne på hjulet
    push();
    rotate(vinkel);
    rect(0,0,0,285);
    rotate(0);
    rect(0,0,0,285);
    rotate(45);
    rect(0,0,0,285);
    rotate(90);
    rect(0,0,0,285);
    rotate(135);
    rect(0,0,0,285);
    vinkel = vinkel + spin;
    pop();

    //holdere på hjulet
    push();
    fill(253);
    ellipse(0,0,253);
    rotate(90);
    rect(157,0,0,313);
    rotate(45);
    rect(0,-40,0,363);
    rotate(-90);
    rect(0,40,0,363);
    pop();

    //hamster
    push();
    strokeWeight(0.5);
    rotate(salto);

    //bævegelse af bagerste forben
    push();
    translate(0,65);
    rotate(ben2);
    arc(23,41,18,50,0,-180);
    ben2 = ben2 + speed2;

    if(ben2 <= -55) {
  speed2 = speed2 * -1;
} else if (ben2 >= -25) {
speed2 = speed2 * -1;
}
    pop();

    //bevægelse af bagerste bagben
    push();
    translate(0,65);
    rotate(ben3);
    arc(-23,42,18,50,0,-180);
    ben3 = ben3 + speed3;

    if(ben3 >= 55) {
  speed3 = speed3 * -1;
} else if (ben3 <= 25) {
speed3 = speed3 * -1;
}
    pop();

    //øre
    arc(23,26,29,25,100,-20);
    //krop
    ellipse(0,65,120,80);
    //hoved
    arc(50,46,60,60,210,75);
    //øre
    arc(20,35,29,25,60,-40);
    //næse
    arc(75,50,25,28,280,100);
    //hale
    arc(-57,55,10,10,80,-40);

    //bævegelse af foreste forben
    push();
    translate(0,65);
    rotate(ben);
    arc(23,39,18,50,0,-180);
    ben = ben + speed;

    if(ben <= -42) {
  speed = speed * -1;
} else if (ben >= -15) {
speed = speed * -1;
}
    pop();

    //bevægelse af forreste bagben
    push();
    translate(0,65);
    rotate(ben4);
    arc(-22,39,18,50,0,-180);
    ben4 = ben4 + speed4;

    if(ben4 >= 42) {
  speed4 = speed4 * -1;
} else if (ben4 <= 15) {
speed4 = speed4 * -1;
}
    pop();

    //næse
    fill(255,204,204);
    ellipse(82,45,15,12);
       //mund
       fill(190);
       ellipse(82,59,2,2);
    //øjne
    eye();

    //pletter
    noStroke();
    fill(245);
    ellipse(-30,45,25,15);
    ellipse(-20,60,15,15);
    fill(212,155,155);
    ellipse(20,35,17,14);
    //dråbe
    fill(100,100,255)
    triangle(40,38,37,48,43,48);
    ellipse(40,48,7,7);

  //når man holder musen inde, laver hamsteren saltoer
    if(mouseIsPressed) {
        salto = salto-12;
   }  else{
      salto = 0;
   }
    pop();

//tekst under hjulet
push();
fill(0,204,204);
strokeWeight(3);
stroke(0,51,102);
textSize(35);
textFont("Sacramento");
text("Loading",-50,220);
//bevægende prikker efter loading
text(dots[index],70,219);
index = index + 1;
if (index == dots.length){
  index = 0;
}
/*strokeWeight(1.5);
for(i = 0 ; i < 50; i += 10){
  ellipse(75 + i, 219, 5, 5);
   }*/
pop();

//hjulet og hamsteren stopper når musen er inden i firkanten
if (mouseX>100 && mouseX<135 && mouseY>100 && mouseY<135){
    spin = 0;
    push();
    rotate(salto);
    //dækning
    noStroke();
    ellipse(40,45,15,15);
    ellipse(82,59,5,5);
    //smil
    stroke(0);
    strokeWeight(0.5);
    noFill();
    arc(82,55,7,7,0,-180);
    pop();
    push();
    noStroke();
    fill(255);
    rect(10,210,250,50);
    pop();

    //teksten ændres også
    push();
fill(0,204,204);
textSize(35);
textFont("Sacramento");
strokeWeight(3);
stroke(0,51,102);
text("Thanks <3",-50,220);
pop();

    ben = -18;
    ben2 = -25;
    ben3 = 25;
    ben4 = 15;

}else{
    spin = -10;
}
push();
//ost
strokeWeight(0.5);
fill(255,255,102);
rect(mouseX-20,mouseY-30,25,25);
fill(100);
ellipse(mouseX-27,mouseY-30,5);
ellipse(mouseX-23,mouseY-37,4);
ellipse(mouseX-15,mouseY-29,5);
pop();
}

//funktionen så øjnene følger musen
function eye (){
    let e_x = map(mouseX,0,255,57,58);
    let e_y = map(mouseY,0,255,35,37);

    fill(255);
    ellipse(60,38,12,15);
    fill(102,153,102);
    ellipse(e_x,e_y,7,7);
}
