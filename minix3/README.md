# minix3
Her er nogle screenshots af mit program:


<img src="minix3/hamter1.png">
<img src="minix3/hamster2.png">
<img src="minix3/hamster3.png">


Link: [Hamsterhjul](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix3/index.html)

Link til kode: [koden](https://gitlab.com/stetisk-programmering/aestetiskprogrammering/-/blob/main/minix3/minix3.js)

# Beskrivelse af programmet
I dette program er der er maaasse rotation. Jeg startede derfor med at med at sette en masse let-statements såsom:
~~~~
let vinkel = 0;
let salto = 0;
let speed = 4;
let ben = -42;
~~~~
Her er der både sat et par vinkler, en hastighed og en placering.
Jeg satte selvfølgelig også mit canvas og min baggrund. Jeg satte anglemode() til DEGRESS, så alle dele roterer korrekt, og rectMode() til CENTER, så alle rektanglers placering ville blive ud fra den midte.

Jeg satte så en firkant med teksten: - mouse here to give me a break ", over firkanten, for at indikere at musen skal være inde for firkanten for at hamsterhjulet stopper. Jeg satte også en tekst for at fortælle man kan få hamsteren til at lave en salto. For dette ser koden således ud:
~~~~
push();
  textSize(15);
  stroke(0);
  strokeWeight(3);
  fill(255);

  textFont("Staatliches");
  text("- mouse here to give me a break <3",20,80);

  textFont("Staatliches");
  text("- hold the mouse to see me spinning",200,40);
  pop();
  ~~~~


  Derefter lavede jeg linjerne på hjulet, som egentlig er rektangler lavet til linjer, så det var nemmere og hurtigere at lave dem med rectMode(CENTER). Jeg satte dem inde i en push/pop(), så de ville få deres egen rotation. Jeg fik den til at rotere ved at skrive:
  ~~~~
  rotate(vinkel);
  //alle rektangler indsat her
  vinkel = vinkel + spin;
  ~~~~
  Jeg lavede hvert ben for sig, med en rotation om kroppen af hamsteren. Det gjorde jeg ved funktionen tranlate(x,y på hamsterkrop).


  Et ben kan se således ud:
  ~~~~
  push();
    translate(0,65);
    rotate(ben2);
    arc(23,41,18,50,0,-180);
    ben2 = ben2 + speed2;

    if(ben2 <= -55) {
  speed2 = speed2 * -1;
} else if (ben2 >= -25) {
speed2 = speed2 * -1;
}
  pop();
~~~~
Her har jeg lavet buer som ben, og fået dem til at bevæge sig frem og tilbage, ved at sætte to punkter hvor hastigheden skifter fortegn hver gang benet når det.


For at få hamsteren til at lave en salto, satte jeg først en rotate(salto), og brugter så et if-statement, hvor at der bliver sat hastighed på når man holder musen inde. Eksempel:
~~~~
if(mouseIsPressed) {
        salto = salto-12;
   }  else{
      salto = 0;
   }
  ~~~~
  For at få prikkerne efter "loading" (som jeg har sat ind ligesom de andre tekster), lavede jeg øverst i koden også et par variabler der så sådan ud:
  ~~~~
  var dots = [".","..","...","...."];
var index = 0;
~~~~
Der er muligvis smartere måder at få prikker til at bevæge sig på samme måde, men det var en god øvelse at prøve mig frem med en array. Jeg skrev at den skulle starte med index, og derefter skifte til næste inde. Efterføgende, et if statement for at starte forfra i arayen.
~~~~
text(dots[index],70,219);
index = index +1;
if (index == dots.length){
  index = 0;
}
~~~~
For at få hamsteren til at tage en pause skrev jeg et if-statement, med et område musen skulle være inde for for at stoppe og ændre animationen.
~~~~
if (mouseX>100 && mouseX<135 && mouseY>100 && mouseY<135){
    spin = 0;
   (ændringer og tilføjelser her........)
}else{
    spin = -10;
}
~~~~
Til sidst har jeg tilføjet at øjenene bevæger sig efter musen, ved at lave en ny funktion kun for øjnene. Jeg lavede et område pupilen kunne bevæge til for både x- og y-akste.
~~~~
function eye (){
    let e_x = map(mouseX,0,255,57,58);
    let e_y = map(mouseY,0,255,35,37);

    fill(255);
    ellipse(60,38,12,15);
    fill(102,153,102);
    ellipse(e_x,e_y,7,7);
}
~~~~

# Reflektering
Temporalitet er for mig er en mærkværdig ting. Jeg føler aldrig man kan gennemskue hvor lang tid noget tager, eller hvor lang tid jeg har brugt på noget. Nogle gange går tiden hurtigt, når man for eksempel har det sjovt eller sidder og arbejder med noget der for en gang skyld er spændende. Andre gange går den utroligt langsomt, og er noget man snakker om skal "slås ihjel". For eksempel havde jeg regnet med at tiden ville gå langsomt med at lave denne minix, men selvom jeg har brugt utrolig og måske alt for lang tid på den, føler jeg det ikke, da jeg synes det har været hyggeligt at sidde og kode og lytte til musik i mange timer. 


Når det kommer til temporalitet og computere, er det dog også noget andet end at sidde med andre ikke-digitale projekter. Computeren har ingen tidsbegrænsning. Hvis man har strøm, kan man sagtens sidde hele natten, og lave ubegrænset forskellige digitale aktiviteter. Det eneste der giver en fornemmelse af den tid man har brugt foran skærmen er de små tal der indikerer hvad klokken er, hvis de overhoved vises samtidig med det der fortages på computeren. Dette er et emne Hans Lammerant diskuterer i sin tekst “How humans and machines negotiate experience of time”.  Her argumenteres for, at computere ingen døgnrytme har, i modsætning til mennesket. "The night is generally the preferred time for sleep, but the availability of artificial light and the need for long-distance coordination has influenced how humans deal with the day cycle and the place for sleep in it" (Lammrant, 2018). Mennesket har brug for at sove om natten hvor det er mørkt, og arbejder i de lyse timer. På en skærm ses der ingen forskel, da man får den samme mængde lys fra den alle tider på døgnet. Hvis man ligger for meget i ske med din telefon eller computer, mon så ikke også det er derfor man hurtig kan få vendt om på dag og nat?


I forhold til mit program, har jeg valgt at lave et hamsterhjul. Dette skal symbolisere hvordan computersystemer kan blive ved og ved med at køre, indtil man interagere med det og stopper det. Selve hamsteren løber i uendelighed, svedende, og stopper først når man vælger at give den en pause. Når man giver den en pause stopper den hypotetiske loading dog, og det vil derfor tage "længere tid" at få loadet. Dette kan perspektiveres til den stigende tendens på arbejdsmarkedet, hvor man på mange områder skal arbejde hurtigere og mere effektivt - altså hvor man skal blive ved med at løbe i hamsterhjulet. Pointen med programmet er derved, at det også er vigtigt man får stoppet op, da vi jo ikke er som computere der kan fortsætte i uendelighed, men mennesker der har brug for hvile og tid til at tænke. 


Littaratur:

• Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018), https://monoskop.org/log/?p=20190

• Wolfgang Ernst, “‘... Else Loop Forever’: The Untimeliness of Media,” (2009), https://www.musikundmedien.hu-berlin.de/de/medienwissenschaft/medientheorien/ernst-in-english/pdfs/medzeit-urbin-eng-ready.pdf 

