let player;
let disco = [];
let score = 0;
let manyDisco = 1;
let level = 1;
let bomb = [];
let manyBomb = 7;
let discosound;

function preload(){
    soundFormats('mp3');
    discosound = loadSound("data/discosound.mp3");
    danceman = loadImage("data/monster.gif");
    discoimg = loadImage("data/confettibomb.png");
    bombeimg = loadImage("data/slime.png");
    dancefloor = loadImage("data/floor.jpg");
    slimemonsterimg = loadImage("data/slimemonster.gif");
  }


function setup(){
    createCanvas(800, windowHeight);
    background(255);
    discosound.play();
    player = new Player();
    slimemonster = new Slimemonster();

}

function draw(){
imageMode(CORNER);
background(255);

push();
tint(255, 100);

image(dancefloor, 0, 0, 500, 500);
image(dancefloor, 500, 0, 500, 500);
image(dancefloor, 0, 500, 500, 500);
image(dancefloor, 500, 500, 500, 500);
pop();

if (keyIsDown(UP_ARROW)){
    player.moveUp();
} if (keyIsDown(DOWN_ARROW)){
    player.moveDown();
}
if (keyIsDown(LEFT_ARROW)){
    player.moveLeft();
} if (keyIsDown(RIGHT_ARROW)){
    player.moveRight();
}


discoNum();
bombNum();
player.show();
player.move();
slimemonster.show();
slimemonster.move();
slimemonster.edges();
showBomb();
showDisco();
collisionCheck();
levelUp();
displayScore();
collisionBombCheck();
collisionSlimeCheck();

}

function keyPressed(){
    if (keyCode === ENTER){
        window.location.reload();
    }
    }

function collisionCheck(){
    for (let i = 0; i < disco.length; i++) {
    let distance = int(dist(player.posX, player.posY, disco[i].posX, disco[i].posY));
    
    console.log("score " + score);

    if (distance < 55){ 
        score++;
        disco.splice(i,1);
        level++;
        manyDisco = manyDisco + 0.3;
        manyBomb = manyBomb + 2;
    }
}
}

function collisionBombCheck(){
    for (let i = 0; i < bomb.length; i++) {
    let distance= int(dist(player.posX, player.posY, bomb[i].posX, bomb[i].posY));

    if (distance < 25){ 
        noLoop();
        fill(255, 0, 0);
        textSize(50);
        text("game over", width/2-80, height/2);
        fill(255);
        textSize(17);
        text("press ENTER to reset", width/2-40, height/1.4);
    }
}
}

function collisionSlimeCheck(){
    
    let distance= int(dist(player.posX, player.posY, slimemonster.posX, slimemonster.posY));

    if (distance < 25){ 
        noLoop();
        fill(255, 0, 0);
        textSize(50);
        text("game over", width/2-80, height/2);
        fill(0);
        textSize(17);
        text("press ENTER to reset", width/2-40, height/1.4);
    }

}

function discoNum() {
    if (disco.length < manyDisco) {
      disco.push(new Disco());
    }
  }

  function showDisco(){
    for (let i = 0; i < disco.length; i++) {
      disco[i].move();
      disco[i].show();
      disco[i].edges();
  
    }
}

function bombNum(){
    if(bomb.length < manyBomb){
        bomb.push(new Bomb());
        }

}

function showBomb(){
    for (let i = 0; i < bomb.length; i++) {
        //bomb[i].move();
        bomb[i].show();
        //bomb[i].edges();
}
}


function levelUp(){
    if (score === 1){
        level = 2;
        manyDisco = 2;
        manyBomb = 10;
        //checkDiscoNum();
       
    } else if(score === 6){
        level = 3;
        manyDisco = 3;
        manyBomb = 15;

    } else if(score === 12){
        level = 4;
        manyDisco = 4;
        manyBomb = 20;

    }else if(score === 18){
        level = 5;
        manyDisco = 5;
        manyBomb = 25;

    }else if(score === 24){
        level = 6;
        manyDisco = 6;
        manyBomb = 30;

    }else if(score === 30){
        level = 7;
        manyDisco = 7;
        manyBomb = 35;

    }else if(score === 40){
        level = 8;
        manyDisco = 8;
        manyBomb = 40;
        
    }else if(score === 50){
        level = 9;
        manyDisco = 9;
        manyBomb = 45;

    }else if(score === 65){
        level = 10;
        manyDisco = 10;
        manyBomb = 50;
    }
    if(score === 100){
        noLoop();
        fill(0, 255, 0);
        textSize(50);
        text("you won!", width/2-80, height/2);
        fill(0);
        textSize(17);
        text("press ENTER to reset", width/2-40, height/1.4);
    }
}

/*
function levelUpUp(){
    if (score === 10 || 20 || 30 || 40){
        level = level + 1;
        manyDisco++;
        manyBomb++;
    } else {

    }
}
*/

function displayScore() {
    fill(0);
    textSize(17);
    textFont("Sacramento");
    text('You have catched '+ score + " bomb(s)", 40, height/1.4);
    text('PRESS the ARROWS to dance around',
    40, height/1.4+40);
    text('Level ' + level, 40, 50);
}
