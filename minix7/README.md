# minix7
Jeg har valgt at genbesøge min minix6, da jeg føler det er den der er mest kompleks, og flest kritiske ting ved. I minix6, var der også nogle funktioner jeg godt kunne tænke mig var anderledes, og derfor tænkte jeg det ville være lærerigt at se, om det kunne lykkes for mig.


Her er et screenshotaf spillet:


<img src="Skærmbillede_2023-05-03_164210.png">

Her er links til spillet og koderne:

[Link til spillet](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix7/index.html)

[Link til koden](https://gitlab.com/stetisk-programmering/aestetiskprogrammering/-/blob/main/minix7/minix7.js)

[Link til Player class koden](https://gitlab.com/stetisk-programmering/aestetiskprogrammering/-/blob/main/minix7/Player.js)

[Link til Disco class koden](https://gitlab.com/stetisk-programmering/aestetiskprogrammering/-/blob/main/minix7/Disco.js)

# koden
De funktioner jeg har haft mest fokus på at ændre, er funktionen der flytter playeren, og så at tilføje en funktion der sørger for at det ikke er muligt at bevæge sig uden for canvas. 

Den første funktion der flytter playeren, fungerede således i den gamle minix, at den lå gemt i classen, og blev kaldt på under en keyPressed-funktion i hovedfilen. Her virkede funktionen fint, men det var kun muligt at flytte playeren ved at klikke på piletasterne, hvor jeg havde tænkt jeg gerne ville have at man bare kunne trykke dem ned.

Dette så sådan ud i classen:
~~~~
    moveUp(){
        this.posY -= this.speed;
    }
    moveDown(){
        this.posY += this.speed;
    }
    moveLeft(){
        this.posX -= this.speed;
    }
    moveRight(){
        this.posX += this.speed;
    }
~~~~
og sådan ud i hovedfilen:
~~~~
function keyPressed() {
    if (keyCode === UP_ARROW){
        player.moveUp();
    } else if (keyCode === DOWN_ARROW){
        player.moveDown();
    }
    if (keyCode === LEFT_ARROW){
        player.moveLeft();
    } else if (keyCode === RIGHT_ARROW){
        player.moveRight();
    }
    if (keyCode === ENTER){
        window.location.reload();
    }
~~~~

For at få playeren til at bevæge jeg, blot når jeg holder tasten nede, fandt jeg frem til at det nok var keyPressed-funktionen, der var problemet, da det er hvad den kan. Derfor slettede jeg funktionen og ændrede det til if-statements, hvor jeg kunne benytte mig af syntaksen "keyIsDown". Her blev dog stadig kaldt på "move"-funktionerne fra classes. Dette kan ses nedenfor:
~~~~
if (keyIsDown(UP_ARROW)){
    player.moveUp();
} if (keyIsDown(DOWN_ARROW)){
    player.moveDown();
}
if (keyIsDown(LEFT_ARROW)){
    player.moveLeft();
} if (keyIsDown(RIGHT_ARROW)){
    player.moveRight();
}
~~~~

For at sørge for at playeren ikke ville bevæe sig ud af canvas, tænke jeg at jeg kunne lave en ny funktion inde under classen, med nogle if-statements om, at hvis man når kanten af canvas, bliver man altså stående. Dette stykke kode ses nedenfor:
~~~~
move(){
        if(this.posX > width){
            this.posX = width - 40
        }
        if(this.posX < 0){
            this.posX = 40
        }
        if(this.posY > height){
            this.posY = height - 40
        }
        if(this.posY < 0){
            this.posY = 40
        }
    }
~~~~


I koden har jeg også fjernet move-funktionen ved objekterne man ikke må røre, så det bare ligger tilfældigt som blokader i spillet. Da spillet blev en smule nemt ved denne ændring, satte jeg så et bevægende objekt ind som "fjende", som havde samme kodning som objekterne man skulle samle. Derfor kan denne funktion ses beskrevet i min minix6, hvis nødvendigt.
# Refleksion
Hvis man skal kigge på metodologien har jeg haft stor fokus i denne minix på at reflektere over valg af objekter, og deres betydning/udtryk det giver. Det er interssant at dykke ned i, hvilke påvirkninger og tanker der opstår, på baggrund af hvilke digtale beslutninger man tager sig. Jeg har oplevet at det kan være lærerigt selv at skulle tage stilling til digital beslutningtagen, da det skaber nye reflektioner omkring bla. hvor vigtigt det er at have et kritisk syn under beslutningstagen vedrørende teknologi.


Udover ændringerne i selve syntakserne, har jeg lavet nogle designændringer. Jeg havde fokus på, at jeg gerne ville have noget barnligt og urealistisk, for at skabe et neutralt indtryk i spillet. Jeg skiftede fx. playeren til at være et dansende gladt væsen, i stedet for en hiphop-man. Jeg ændrede bomberne til at være grønt slim i stedet, da det er mere neutralt også, og blot skal illustrere noget klistet man ikke skal røre, og ikke bomber hvor man dør hvis man rører dem. Diskokuglerne man skulle fange i det gamlespil, ændrede jeg til konfettibomber, og er nok det der er mest kritisk ved dette spil. Grunden til jeg har valgt konfettibomber som en positiv ting man skal fange, er for at vise ideen om hvordan opfattelser af objekter kan være forskellige. Konfettibomberne er lyserøde og har øjne og ben, hvilket skaber et udtryk der siger at det ikke er noget farligt, men alligvel kommer det an på konteksten af spillet, om det er noget godt eller dårligt. Stadig har jeg leget med tanken om, at vende det negative til noget positivt, og kunne måske have ungået at vælge et andet billede af en bombe for at illustrere dette, da det i bund og grund ikke er relevant. 
Jeg har lært i denne minix, at der er mange vinkler og apspekter af, hvilket udtryk digitale valg giver. Det er netop her man kan snakke om digital kultur, at vores opfattelser og antagelser af ting vi møder i teknologien, ofte er præget af hvilken digital kultur vi er tilhængere af. Relationen mellem æstetisk programmering og digital kultur har flere apspekter, men en vigtig pointe er at have et kritisk og åbent syn på den teknologi vi har, og hvilket relation og indvirkning den kan have på den digitale kultur. Dette undersøtte også vigtigheden af dette. For at gentage mig selv fra min README til min minix6: "Dét at vi lever i et digitaliseret samfund, hvor så lille en gruppe eksperter har magten over udviklingen af teknologi, begrænser på en måde potentialet for demokratisk deltagelse i "beslutningsprocesser" vedrørende teknologi i vores samfund. Denne problematik minder mig om spørgsmålet omkring hvorvidt alle delvist burde lære hvordan man programmerer, og dette emne kan argumentere godt for, at løsningen måske er at alle bør kende til programmering."

Gennem arbejdet med denne minix, og mine andre, har jeg kunne danne et overblik over forskellige vinkler på teknologi, og dermed kunne blive bedre til at reflektere over konceptet med æstetisk programmering. Som Soon & Cox skriver i indledningen af deres bog Aestetic Programming "It follows the principle that the growing importance of software requires a new kind of cultural thinking - and curriculum - that can account for, and with which to better understand the politivs and aestetics of algorithmic procedures, data processing and abstraction". Dette har jeg lært og forstået på et andet niveau, ved selv at sidde med programmering i hænderne, ift. at læse det i en bog.


Litteratur:


Winnie Soon, Geoff Cox,Astetic Programming - a handbook of software studies, Opehn Humanities Press, 2020
