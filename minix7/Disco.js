console.log("discodasco");

class Disco{

    constructor(){
        this.posX = random(0, width);
        this.posY = random(0, height);
        this.speedX = 3;
        this.speedY = 2;
        this.sizeX = 40;
        this.sizeY = 40;
        
    }

    show(){
        image(discoimg,this.posX,this.posY, this.sizeX, this.sizeY);
    }

    move(){
        this.posX += this.speedX;
       
        this.posY += this.speedY;
       
    }

    edges(){
    if(this.posX > width || this.posX < 0){
        this.speedX *= -1;
        }
    if(this.posY > height || this.posY < 0){
        this.speedY *= -1;
  }
}
}

class Bomb{

    constructor(){
        this.posX = random(0, width);
        this.posY = random(0, height);
        this.speedX = 3;
        this.speedY = 2;
        this.sizeX = 40;
        this.sizeY = 40;
   
    }

    show(){
        image(bombeimg,this.posX,this.posY, this.sizeX, this.sizeY);
    }

    move(){
        this.posX += this.speedX;
        this.posY += this.speedY;
    }

    edges(){
    if(this.posX > width || this.posX < 0){
        this.speedX *= -1;
        }
    if(this.posY > height || this.posY < 0){
        this.speedY *= -1;
  }
}
}

class Slimemonster{

    constructor(){
        this.posX = random(0, width);
        this.posY = random(0, height);
        this.speedX = 3;
        this.speedY = 2;
        this.sizeX = 150;
        this.sizeY = 150;
   
    }

    show(){
        imageMode(CENTER);
        image(slimemonsterimg,this.posX,this.posY, this.sizeX, this.sizeY);
    }

    move(){
        this.posX += this.speedX;
        this.posY += this.speedY;
    }

    edges(){
    if(this.posX > width || this.posX < 0){
        this.speedX *= -1;
        }
    if(this.posY > height || this.posY < 0){
        this.speedY *= -1;
  }
}
}