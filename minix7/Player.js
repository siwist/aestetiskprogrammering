console.log("danceman");

class Player{

    constructor(){
        rectMode(CENTER);
        imageMode(CENTER);
        this.posX = width/2;
        this.posY = height/2;
        this.speed = 3;
        this.sizeX = 100;
        this.sizeY = 100;
    }

    show(){
        noFill();
        //hjælpelinjer for at lokalisere player
        //rect(this.posX,this.posY, this.sizeX, this.sizeY);
        //rect(this.posX,this.posY, this.sizeX/2, this.sizeY/2);
        imageMode(CENTER);
        image(danceman,this.posX,this.posY, this.sizeX, this.sizeY);
        
    }

    move(){
        if(this.posX > width){
            this.posX = width - 40
        }
        if(this.posX < 0){
            this.posX = 40
        }
        if(this.posY > height){
            this.posY = height - 40
        }
        if(this.posY < 0){
            this.posY = 40
        }
    }

    moveUp(){
        this.posY -= this.speed;
    }

    moveDown(){
        this.posY += this.speed;
    }

    moveLeft(){
        this.posX -= this.speed;
    }

    moveRight(){
        this.posX += this.speed;
    }
}