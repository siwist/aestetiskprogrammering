console.log("discodasco");

class Disco{

    constructor(){
        this.posX = random(0, width);
        this.posY = random(0, height);
        this.speedX = 3;///random(1, 2);
        this.speedY = 2;//random(-)
        this.sizeX = 20;
        this.sizeY = 20;
        //this.vel = p5.vector.random2D();
    }

    show(){
        image(discoimg,this.posX,this.posY, this.sizeX, this.sizeY);
    }

    move(){
        this.posX += this.speedX;
        //if(this.posX > width){
          //  this.posX = 0;
       // }
        this.posY += this.speedY;
        //if(this.posY > height){
          //  this.posY = 0;
        //}
    }

    edges(){
    if(this.posX > width || this.posX < 0){
        this.speedX *= -1;
        }
    if(this.posY > height || this.posY < 0){
        this.speedY *= -1;
  }
}
}

class Bomb{

    constructor(){
        this.posX = random(0, width);
        this.posY = random(0, height);
        this.speedX = 3;///random(1, 2);
        this.speedY = 2;//random(-)
        this.sizeX = 22;
        this.sizeY = 20;
        //this.vel = p5.vector.random2D();
    }

    show(){
        image(bombeimg,this.posX,this.posY, this.sizeX, this.sizeY);
    }

    move(){
        this.posX += this.speedX;
        //if(this.posX > width){
          //  this.posX = 0;
       // }
        this.posY += this.speedY;
        //if(this.posY > height){
          //  this.posY = 0;
        //}
    }

    edges(){
    if(this.posX > width || this.posX < 0){
        this.speedX *= -1;
        }
    if(this.posY > height || this.posY < 0){
        this.speedY *= -1;
  }
}
}