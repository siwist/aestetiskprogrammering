let player;
let disco = [];
let score = 0;
let manyDisco = 1;
let level = 1;
let r;
let g;
let b;
let a;
let bomb = [];
let manyBomb = 1;
let discosound;

function preload(){
    soundFormats('mp3');
    discosound = loadSound("data/discosound.mp3");
    danceman = loadImage("data/dansemand.gif");
    discoimg = loadImage("data/diskokugle.png");
    bombeimg = loadImage("data/bombe.png");
    dancefloor = loadImage("data/dancefloor.png");
  }


function setup(){
    createCanvas(800, windowHeight);
    background(255);
    discosound.play();
    player = new Player();

}

function draw(){
imageMode(CENTER);
image(dancefloor, width/2, height/2, width, height);

discoNum();
bombNum();
player.show();
showBomb();
showDisco();
collisionCheck();
levelUp();
displayScore();
collisionBombCheck();


}

function collisionCheck(){
    for (let i = 0; i < disco.length; i++) {
    //let distance;
    let distance = int(dist(player.posX, player.posY, disco[i].posX, disco[i].posY));
    
    console.log("score " + score);

    if (distance < 55){
        score++;
        disco.splice(i,1);
       
    }
}
}

function collisionBombCheck(){
    for (let i = 0; i < bomb.length; i++) {
    let distance= int(dist(player.posX, player.posY, bomb[i].posX, bomb[i].posY));

    if (distance < 25){ 
        noLoop();
        fill(255, 0, 0);
        textSize(50);
        text("game over", width/2-80, height/2);
        fill(255);
        textSize(17);
        text("press ENTER to reset", width/2-40, height/1.4);
    }
}
}

function discoNum() {
    if (disco.length < manyDisco) {
      disco.push(new Disco());
    }
  }

  function showDisco(){
    for (let i = 0; i < disco.length; i++) {
      disco[i].move();
      disco[i].show();
      disco[i].edges();
  
    }
}

function bombNum(){
    if(bomb.length < manyBomb){
        bomb.push(new Bomb());
        }

}

function showBomb(){
    for (let i = 0; i < bomb.length; i++) {
        bomb[i].move();
        bomb[i].show();
        bomb[i].edges();
}
}

function keyPressed() {
    if (keyCode === UP_ARROW){
        player.moveUp();
    } else if (keyCode === DOWN_ARROW){
        player.moveDown();
    }
    if (keyCode === LEFT_ARROW){
        player.moveLeft();
    } else if (keyCode === RIGHT_ARROW){
        player.moveRight();
    }

    if (keyCode === ENTER){
        window.location.reload();
    }

    return false;

}


function levelUp(){
    if (score === 1){
        level = 2;
        manyDisco = 2;
        //checkDiscoNum();
       
    } else if(score === 3){
        level = 3;
        manyDisco = 3;
        //manyBomb = 3;

    } else if(score === 6){
        level = 4;
        manyDisco = 4;
        manyBomb = 3;

    }else if(score === 10){
        level = 5;
        manyDisco = 5;
        manyBomb = 4;

    }else if(score === 15){
        level = 6;
        manyDisco = 6;
        //manyBomb = 4;

    }else if(score === 21){
        level = 7;
        manyDisco = 7;
        manyBomb = 5;

    }else if(score === 28){
        level = 8;
        manyDisco = 8;
        manyBomb = 6;
        
    }else if(score === 35){
        level = 9;
        manyDisco = 9;
        manyBomb = 7;

    }else if(score === 42){
        level = 10;
        manyDisco = 10;
        manyBomb = 8;
    }
    if(score === 43){
        noLoop();
        fill(0, 255, 0);
        textSize(50);
        text("you won!", width/2-80, height/2);
        fill(255);
        textSize(17);
        text("press ENTER to reset", width/2-40, height/1.4);
    }
}

function displayScore() {
    fill(255);
    textSize(17);
    textFont("Sacramento");
    text('You have catched '+ score + " disco ball(s)", 40, height/1.4);
    text('PRESS the ARROWS to dance around',
    40, height/1.4+40);
    text('Level ' + level, 40, 50);
}
