console.log("danceman");

class Player{

    constructor(){
        rectMode(CENTER);
        imageMode(CENTER);
        this.posX = width/2;
        this.posY = height/2;
        this.speed = 20;
        this.sizeX = 120;
        this.sizeY = 120;
    }

    show(){
        noFill();
        //hjælpelinjer for at lokalisere player
        //rect(this.posX,this.posY, this.sizeX, this.sizeY);
        //rect(this.posX,this.posY, this.sizeX/2, this.sizeY/2);
        image(danceman,this.posX,this.posY, this.sizeX, this.sizeY);
       
    }

    moveUp(){
        this.posY -= this.speed;
    }

    moveDown(){
        this.posY += this.speed;
    }

    moveLeft(){
        this.posX -= this.speed;
    }

    moveRight(){
        this.posX += this.speed;
    }
}