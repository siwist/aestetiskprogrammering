# minix6
Her er et par screenshots af mit spil:

<img src="play.png">

<img src="gameover.png">

<img src="youwon.png">

Her er mine koder [sketch](https://gitlab.com/stetisk-programmering/aestetiskprogrammering/-/blob/main/minix6/minix6.js), [Player](https://gitlab.com/stetisk-programmering/aestetiskprogrammering/-/blob/main/minix6/Player.js), [Objekter](https://gitlab.com/stetisk-programmering/aestetiskprogrammering/-/blob/main/minix6/Disco.js).



Her er et link til mit program: [The Disco Game](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix6/index.html)

# Beskrivelse af spillet
Spillet går ud på at man er dansende mand der skal fange diskokugler, samtidig med at man skal undgå bomberne. Man starter i level 1 hvor der én diskokugle og én bombe. Når man så har fanget diskokuglen kommer der en diskokugle mere, og man skal nu fange to diskokugler for at komme et level op, men også undgå to bomber. Spillet bliver derfor svære og svære jo højere level man kommer i, da der kommer til at være mange bombre man skal undgå. Man flytter manden ved at bruge alle fire piletaster.

Den dansende mand flytter med piletasterne for at fange diskokuglerne. Han rykker sig med 20 pixels pr. klik. Diskokuglerne bevæger sig alle i samme hastighed, og bevæger sig fra "kant" til "kant" på canvas, så de ikke forsvinder. Det samme gør bomberne. Diskokuglerne egenskab er at de giver et point hver gang man fanger en. For hvert level skal man fange det antal diskokugler som det antal level man er i, for at gå et level op. Der kommer flere og flere diskokugler, samt bomber, for hvert level. Hvis man kommer til at røre en bombe taber man spillet og skal starte forfra.



# Koden
Jeg vil starte med at vise hvordan min Player Class er bygget op. Jeg startede ned en contructor, hvor jeg definerede positionerne, størrelse, og hastigheden. Jeg har også indtillet rect og image til at have origin i midten. Grunden til jeg også har rectMode(CENTER) med, er fordi jeg senere hen har indsat nogle "hjælpe-firkanter" rundt om selve playeren, da det godt kunne være lidt svært at lokalisere den, da jeg har valgt en gif som player.
~~~~
class Player{
    constructor(){
        rectMode(CENTER);
        imageMode(CENTER);
        this.posX = width/2;
        this.posY = height/2;
        this.speed = 20;
        this.sizeX = 120;
        this.sizeY = 120;
    }
~~~~
Herunder ses show-funktionen med hjælpelinjerne jeg har fjernet tilsidst, og min gif jeg har indsat i "image". Her er giffen defineret i "hovedsketchen", hvor den er kaldt "danceman". Her ses også positioner og størrelser for player.
~~~~
    show(){
        noFill();
        //hjælpelinjer for at lokalisere player
        //rect(this.posX,this.posY, this.sizeX, this.sizeY);
        //rect(this.posX,this.posY, this.sizeX/2, this.sizeY/2);
        image(danceman,this.posX,this.posY, this.sizeX, this.sizeY);
    }
~~~~
Derefter har jeg tilføjet bevægelses-funktioner, som bliver kaldt i en funktion i "hovedsketchen". Her har jeg fx. lavet en moveUp, hvor y-postitionen af player bliver rykket up (minusset), med -this.speed. Jeg har både lavet en moveUp, moveDown, moveLeft og en moveRight- funktion der rykker på player. (se min kode for de andre)
~~~~
    moveUp(){
        this.posY -= this.speed;
    }
~~~~


Jeg viser kun "classen" for diskokuglerne, da bombernes er identisk. I constructor- og move- funktionen har jeg defineret det samme som i player-classen, dog har jeg her sat x- og y-positionerne til at være et tilfædigt sted på canvas ved at sige sådan:
~~~~
        this.posX = random(0, width);
        this.posY = random(0, height);
~~~~

Jeg har dog med diskokuglerne og bomberne også æavet en funktion der hedder edges. Denne sørger for at når x- eller y- positionen af objektet når ud for canvas, vil hastigheden ændre fortegn, og objektet ændre derfor retning. Det er denne effekt der minder om spillet pingpong som nævnt tidligere. Funktionen ser således ud:
~~~~
    edges(){
    if(this.posX > width || this.posX < 0){
        this.speedX *= -1;
        }
    if(this.posY > height || this.posY < 0){
        this.speedY *= -1;
  }
~~~~


Til min "hovedsketch" vil jeg herunder gennemgå de vigtigste elementer. Jeg startede med min globale variabler, hvor jeg her har disco og bomb som tomme arrays.
~~~~
let player;
let disco = [];
let score = 0;
let manyDisco = 1;
let level = 1;
let bomb = [];
let manyBomb = 1;
let discosound;
~~~~

Herefter satte jeg i mit setup min discolyd ind, samt defineret player til at aktivere min PLayer-class.

~~~~
function setup(){
    createCanvas(800, windowHeight);
    background(255);
    discosound.play();
    player = new Player();
}
~~~~

I draw, satte jeg i stedet for en background, et billede. Derefter "kaldte" jeg alle de funktioner jeg har lavet.
~~~~
function draw(){
imageMode(CENTER);
image(dancefloor, width/2, height/2, width, height);

discoNum();
bombNum();
..... og alle andre funktioner
}
~~~~

For at checke om man har fanget en diskukugle, har jeg her brugt det tomme array ved at lave et forloop, hvor i bliver defineret. Ved så at sætte i ind i arrayet, gør det at det gælder alle diskokugler. Herefter har jeg defineret distancen. Int() har vidst noget at gøre med arrayet, jeg er faktisk lidt i tvivl om hvad den helt præcist gør.. Men ellers har jeg brugt funktionen dist, som kan definere en afstand. Derefetr har jeg gjordt så hvis afstanden er mindre en 55, vil discokuglen man har rørt "splices" hvilket betyder at den skal slettes, og scores vil gå op. Jeg har stort set gjordt det samme i min funktion der chekcer bomberne, her stoppes spillet bare når man rører en bombe istedet.
~~~~
function collisionCheck(){
    for (let i = 0; i < disco.length; i++) {
    let distance = int(dist(player.posX, player.posY, disco[i].posX, disco[i].posY));

    if (distance < 55){ 
        score++;
        disco.splice(i,1);
    }
}
}
~~~~

Funktionen discoNum(), sørger for at lave en ny discokugle, hver gang antallet er mindre en den værdi manyDisco er sat til. Funtionen showDisco, kalder på de tre funktioner jeg har lavet i classen. Her gør det tomme array igen, at alle diskokugler for samme funktion. Jeg har lavet to lignede funktioner til bomberne.
~~~~
function discoNum() {
    if (disco.length < manyDisco) {
      disco.push(new Disco());
    }
  }

  function showDisco(){
    for (let i = 0; i < disco.length; i++) {
      disco[i].move();
      disco[i].show();
      disco[i].edges();
  
    }
}
~~~~

I funktionen keyPressed har jeg lavet if-statement ud fra når der bliver trykket på piletasterne. Her kalder hver piletast på de funktioner jeg har beskrevet tidligere der ligger i Player classen.
~~~~
function keyPressed() {
    if (keyCode === UP_ARROW){
        player.moveUp();
    } else if (keyCode === DOWN_ARROW){
        player.moveDown();
    }
 ~~~~

I min funktion levelUp(), har jeg måske gjordt det lidt besværligt da jeg har lavet et if-statement pr level. Men på den måde har jeg kunne holde styr på præcis hvor mange diskokugler og bomber jeg ville have i hvert level. I bunden af denne funktion, har jeg gjordt så man vinder spillet når man har samlet 43 diskokugler, som er lige efter man er kommet i level 10. Man kan hvis man trykker ENTER, refresh hele vinduet og spillet spillet igen. Det samme kan man hvis man taber.
~~~~
function levelUp(){
    if (score === 1){
        level = 2;
        manyDisco = 2;
        //checkDiscoNum();
       
    } else if(score === 3){
        level = 3;
        manyDisco = 3;
        //manyBomb = 3;

 ....... (alle de andre levels)
 
    }
    if(score === 43){
        noLoop();
        fill(0, 255, 0);
        textSize(50);
        text("you won!", width/2-80, height/2);
        fill(255);
        textSize(17);
        text("press ENTER to reset", width/2-40, height/1.4);
    }
}
~~~~

For at få scoren og levellet vist i mit spil, har jeg sat den definerede score og level ind i min tekst ved at skrive +.
~~~~
function displayScore() {
    fill(255);
    textSize(17);
    textFont("Sacramento");
    text('You have catched '+ score + " disco ball(s)", 40, height/1.4);
    text('PRESS the ARROWS to dance around',
    40, height/1.4+40);
    text('Level ' + level, 40, 50);
}
~~~~


# Refleksion
Tanken bag mit spil har været, at det skulle være lidt inspireret af hvordan spil så ud i 90'erne. Grafikken på alle objekter og baggrunden er ikke helt skarp, hvilket får spillet til at se lidt gammelt ud. Selve designet af spillet synes jeg selv minder om disco/hiphop iden i 90'erne, hvor man også begyndte at kunne spille computerspil. Et af de spil man fx. kunne spille i 90'erne, var bla. Pong , som objekternes egenskaber i mit spil også er inspireret af, da bolden i Pong også bevæger sig fra kan til kant. Jeg brugte desuden en video fra Danial Shiffman, hvor han viste hvordan man programmerede et lignende spil til Pong, som hjalp mig med at programmere mine objekter til at have samme egenskab. 


I mit spil ved man ikke hvad objekterne gør, udover at jeg har indsat en sætning i spillet om hvordan man bruger playeren. Dog er det muligt at man fra start af kan regne objekternes egenskaberne ud. Godt nok står diskokuglerne som pointscore, men en diskokugle bliver generelt ofte forbundet med en positiv ting. Bomberne derimod, står der intet om, men kan både være et indlært negativt objekt fra spilverdenen, og kan ses som et negativt objekt i den virkelige verden. Derfor er det ikke nødvendigvis vigtigt at skrive hvad disse gør, fordi der er en chance for at man kan regne ud hvad de gør, dog muligvis kun når de står i kontrast til diskokuglerne. Bomber kan netop også udtrykke noget positivt, hvis man som eksempel anser det som et våben, der dermed kan gøre dig mere magtfuld, eller måske konfettibomber hvis det skulle passe til mit spil.
Derfor kan det diskuteres hvad elementerne udtrykker, især bomben, da der er mange forskellige måder man kan anskue objekters betydning på. For mig valgte jeg at lave bomben, da jeg selv ser det som noget genkendeligt, da som barn brugte timevis på diverse lignende spil, såsom Mine Sweeper hvor man skal undgå bomberne, eller Bomb It hvor man både skal undgå bomberne, men også kan samle dem op som et våben. Derfor er det vigtigt at overveje grundigt hvilke elementer man vælger at have med at gøre i et spil, da det, ligesom det har gjort med mig, kan skabe et indlært syn på objekter. Det kan for eksempel på længere sigt have en negativ indflydelse på børn, der spiller computerspil med våben, da det kan skabe en forkert opfattelse og forståelse af våben – måske at det er nemt, tilgængeligt og magtfuld at have med at gøre?


I forbindelse med dette emne, finder jeg det er interessant endelig at forstå (i hvert fald en lille del af) hvordan et spil (simpelt spil) fungerer. Det har altid været en gåde for mig hvordan spil, apps osv. er sat sammen. Det at forstå dette, er også vigtig ting at tage højde for ift. objektorienteret programmering. Dette bliver benævnt i teksten “The Obscure Objects of Object Orientation” af Matthew Fuller and Andrew Goffeyer. Fuller og Goffeyer kommer i deres tekst ind på hvordan OOP har muliggjort "black-boxing" af teknologi. OOP er et programmeringsparadigme, hvor man tilføjer ekstra abstraktionslag til hvordan vi forstår den teknologiske verden, hvilket gør det vanskeligt at gennemskue den teknologiske struktur der ligger bag, altså er gemt væk i små ”pakker”. Dette kan ses som en problematik, da de underliggende funktioner i software og hardware på den måde skjules for brugeren, da det bliver for abstrakt, hvilket kan skabe en følelse af mystik og eller mistillid til teknologi, hvilket jeg også har oplevet. Som et resultat af dette kan brugere føle sig mere afhængige af tech-eksperter for at hjælpe dem med at finde rundt i og løse problemer med teknologi, hvilket bla. kan forstærke eksisterende magtdynamikker . Denne pointe er noget jeg ikke har tænkt over før, men noget som giver mening, har en stor indflydelse OOP har haft på vores samfund, både kulturelt og politisk. Dét at vi lever i et digitaliseret samfund, hvor så lille en gruppe eksperter har magten over udviklingen af teknologi, begrænser på en måde potentialet for demokratisk deltagelse i "beslutningsprocesser" vedrørende teknologi i vores samfund. Denne problematik minder mig om spørgsmålet omkring hvorvidt alle delvist burde lære hvordan man programmerer, og dette emne kan argumentere godt for, at løsningen måske er at alle bør kende til programmering.


Litteratur:

Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017).

Daniel Shiffman, The Coding Train, Coding Challenge #67: Pong!, 2017, https://www.youtube.com/watch?v=IIrC5Qcb2G4
