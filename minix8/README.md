# minix8

## Individuel del:
I min individuelle del af denne minix, har jeg lavet et flowchart for min miniX6. Vi skulle lave en flowchart for den minix der var mest komplekst, og derfor har jeg valgt at lave en flowchart over mit spil. Mit spil går ud på at man styrer en dansende mand med piletasterne, hvor man skal fange diskukugler for at få point. I spillet er der hele tiden ligeså mange bomber som diskokugler, så det er derfor vigtigt at man bevæger sig rundt. Hvis man bliver ramt af en bombe taber man spillet, og hvis man kommer i level 10 vinder man spillet. I min flowchart har jeg valgt at fokusere på hvordan man spiller spillet, og hvilke konsekvenser der er hvis man ikke aktivt prøver at fange diskokuglerne.


Min Flowchart ser således ud:


![](Flowchart.png)


[Link til Gustavs individuelle del](https://gitlab.com/gustavfe/aestetiskprogrammering/-/tree/main/miniX8?fbclid=IwAR1SlGLNa8yB5q9yeQxux7leZ_29bqL21lPq8E0b2Tt-Qcqp6RC28P4wUf0)


[Link til Ann-Katrines individuelle del](https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/blob/main/MiniX8/ReadMe.md)


## Gruppe del
(Gustav, Ann-Katrine og Sif)


Kan ses på Ann-Katrines gitlab:

https://gitlab.com/Ann-Katrine/aestetisk-programmering/-/tree/main/MiniX8
