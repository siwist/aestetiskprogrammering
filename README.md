# Æstetisk Programmering
Velkommen til min Gitlab:))

# Mine Programmer
[Farvede bobler](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix1/index.html)

[Emoji 1](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix2/index.html)
[Emoji 2](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix2/index2.html)

[Hamsterhjul](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix3/index.html)

[MoneyApp](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix4/index.html)

[Scrabble](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix5/index.html)

[Disco Game](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix6/index.html)


