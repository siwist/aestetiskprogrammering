let knap;
let mic;
let capture;
let lyd;
let billedetaget = false;
let moneys = ["2.8 $","5.2 $","0.98 $","4.1 $","5.1 $","0.22 $","1.3 $","0.79 $"];
let knap2;
let billedesolgt = false;
let lyd2;

function preload(){
    soundFormats('mp3');
    lyd = loadSound('photolyd.mp3');
    lyd2 = loadSound('pengelyd.mp3');
}

function setup(){
    createCanvas(windowWidth,windowHeight)
    background(255);
    knap = createButton("take photo");
    knap.style("backgroundColor","#666666");
    knap.style("color","#000000");
    knap.position(windowWidth/2-25,550);
    knap.size(80,35);

    knap.mouseOver(skifteFarve);
    knap.mousePressed(trykknap);
    knap.mouseOut(resetFarve);
    
    mic = new p5.AudioIn();
    mic.start();

    capture = createCapture(VIDEO);
    capture.size(width,height);
    //gemmer punkterne
    capture.hide();

    ctracker = new clm.tracker();
    //ved ikke hvad det her er
    ctracker.init(pModel);
    ctracker.start(capture.elt);
}
function draw(){
    background(255);
    let volume = mic.getLevel();
    let mappedVolume = map(volume,0,1,0,500);
    console.log(volume);

  

    let positioner = ctracker.getCurrentPosition();
    
    image(capture,width/2-300,height/2-280,600,550);

    //den sorte ramme
    push();
    fill(0);
    rect(0,0,width,75);
    rect(0,height-100,width,100);
    rect(0,0,width/2-150,height);
    rect(width/2+175,0,width,height);
    //iphone
    translate(width/2, height/2);
    fill(255);
    rect(-175,-300,30,600);
    rect(175,-300,30,600);
    rect(-175,-340,380,69,20);
    rect(-175,260,380,69,20);
    noStroke();
    rect(-174.5,-300,28,600);
    rect(176.5,-300,28,600);
    fill(50);
    rect(-4,-306,45,5,30)
    circle(-20,-304,8);
    noFill();
    stroke(0);
    circle(18,294,40);
    rect(10,286,16);
    pop();


    if(positioner.length){
        if(billedetaget == true){
            noLoop();
            stroke(255);
            textSize(22);
            text("Sold for", width/2-45,height/2+160);
            text(random(moneys),width/2+35,height/2+160);
            knap.hide();
            knap2 = createButton("take money");
            knap2.style("backgroundColor","#666666");
            knap2.style("color","#000000");
            knap2.position(windowWidth/2-25,550);
            knap2.size(80,35);
            knap2.mouseOver(skifteFarve);
            knap2.mousePressed(trykknap2);
            knap2.mouseOut(resetFarve);

            
           // if(billedesolgt == true){
            //    window.location.reload();
           // }
        }
        
     else{   
        textSize(15);
        text("smile more",positioner[2][0]-50,positioner[2][1]);
        text("look happier",positioner[10][0]-50,positioner[10][1]);
        text("try some makeup",positioner[21][0]-50,positioner[21][1]-25);
    //skal ind i et array og gøres til en random funktion
    if (mouseX>width/2-25 && mouseX<width/2+55 && mouseY>width/2+150 && mouseY<width/2+182){
        text("take some clothes of",positioner[7][0]-100,positioner[7][1]);
    }
}
    }

}

function skifteFarve(){
    knap.style("backgroundColor","#999999");
    userStartAudio();
    knap2.style("backgroundColor","#999999");

}
function resetFarve(){
    knap.style("backgroundColor","#666666");
    knap2.style("backgroundColor","#666666");

    }

function trykknap(){
    lyd.play();
    billedetaget = true
}

function trykknap2(){
    lyd2.play();
    //billedesolgt = true
    window.location.reload();
    
            
}

    

    //---programmet hedder moneymaker, som skal forestille en app med en AI der guider dig til at få taget et billede der bliver solgt med det samme.

