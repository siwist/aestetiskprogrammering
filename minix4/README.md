# minix3
Her er et par screenshots af mit program:))))) :


<img src="Skærmbillede_2023-03-12_234450.png">



<img src="Skærmbillede_2023-03-12_234507.png">

Her er min [kode](https://gitlab.com/stetisk-programmering/aestetiskprogrammering/-/blob/main/minix4/minix4.js)

Her er et link til mit program: [moneyApp](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix4/index.html)
# Beskrivelse af programmet
Jeg startede med at lave nogle "let"-statements såsom:
~~~~
let knap;
let mic;
let capture;
let lyd;
~~~~
Jeg satte også nogle af dem til at være falsk, hvilket jeg forklarer årsagen til senere. eks.
~~~~
let billedetaget = false;
~~~~
Jeg lavede også et array, jeg skulle bruge til en random funktion senere hen:
~~~~
let moneys = ["2.8 $","5.2 $", ........"];
~~~~
Derefter brugte jeg funktionen ~~~~function preLoad()~~~~, og satte lydformatet samt de to lyde ind jeg havde uploaded til minix4-mappen. eks. 
~~~~ 
soundFormats('mp3');
lyd = loadSound('photolyd.mp3');
lyd2 = loadSound('pengelyd.mp3');
~~~~
I min setup, satte jeg min canvas og min baggrund, og lavede en knap ved at skrive ~~~~knap = createButton("take photo");~~~~. Jeg satte også nogle indstillinger til knappen ved at skrive knap.njwejw og så eks farve i parantes efter.


Derefter satte jeg mikrofon og video. Jeg prøvede egentlig at fjerne mikrofonen, da jeg ikke skulle bruge den alligevel, men så virkede mit kamera ikke. Måske de skal være der begge to, bare så det ene fungerer. Det gjorde jeg således:
~~~~
  mic = new p5.AudioIn();
    mic.start();

    capture = createCapture(VIDEO);
    capture.size(width,height);
    //gemmer punkterne
    capture.hide();

    ctracker = new clm.tracker();
    //ved ikke hvad det her er
    ctracker.init(pModel);
    ctracker.start(capture.elt);
~~~~


Jeg lagde lige mærke til at jeg har dette stående:
~~~~
 background(255);
    let volume = mic.getLevel();
    let mappedVolume = map(volume,0,1,0,500);
    console.log(volume);
~~~~
og det er nok derfor jeg ikke kunne slette mirkofonen....



Jeg satte trackeren til at finde punkterne på ansigtet: 
~~~~  
    let positioner = ctracker.getCurrentPosition();
    image(capture,width/2-300,height/2-280,600,550);
~~~~

Derefter tegnede jeg iphonen, med en sorte ramme omkring. Jeg havde ikke her tænkt over at jeg kunne have startet med dette, og så sat videokameraet oven på dette med mindre mål.

Jeg lavede derefter et if-statement, der blev aktiveret hvis billedetaget = true. billedetaget = true har jeg sat under en funktion jeg har kaldt trykknap, hvor en photolyd også bliver spillet. Denne funktion bliver aktiveret af at man trykker på knappen hvor der står "take photo". Jeg lavede også et else-statement, hvor at der så kommer tekst op på nogle punkter på ansigtet, og mere tekst hvis musen er over knappen. Alt dette ser således ud:
~~~~

    if(positioner.length){
        if(billedetaget == true){
            noLoop();
            stroke(255);
            textSize(22);
            text("Sold for", width/2-45,height/2+160);
            text(random(moneys),width/2+35,height/2+160);
            knap.hide();
            knap2 = createButton("take money");
            knap2.style("backgroundColor","#666666");
            knap2.style("color","#000000");
            knap2.position(windowWidth/2-25,550);
            knap2.size(80,35);
            knap2.mouseOver(skifteFarve);
            knap2.mousePressed(trykknap2);
            knap2.mouseOut(resetFarve);

            
            if(billedesolgt == true){
             
            }
        }
        
     else{   
        textSize(15);
        text("smile more",positioner[2][0]-50,positioner[2][1]);
        text("look happier",positioner[10][0]-50,positioner[10][1]);
        text("try some makeup",positioner[21][0]-50,positioner[21][1]-25);
    //skal ind i et array og gøres til en random funktion
    if (mouseX>width/2-25 && mouseX<width/2+55 && mouseY>width/2+150 && mouseY<width/2+182){
        text("take some clothes of",positioner[7][0]-100,positioner[7][1]);
    }
}
    }
~~~~
Grunden til at der står positioner.length i første if statement, er til at få teksten frem hvis punkterne scannes.



Jeg lavede også et par funktioner, hvor at de blev aktiveret alt efter om musen var over knappen eller ej.
~~~~
function skifteFarve(){
    knap.style("backgroundColor","#999999");
    userStartAudio();
    knap2.style("backgroundColor","#999999");

}
function resetFarve(){
    knap.style("backgroundColor","#666666");
    knap2.style("backgroundColor","#666666");

    }

~~~~
# Reflektering
Med mit program har jeg prøvet at skabe noget, der kan relatere til det emne vi både har om i æstetisk programmering og software studier om data, og de problematikker der findes i forlængelse af dette emne. Ideen med mit program er inspireret af en diskussion der har været i medierne om et nyt filter man kan benytte på tiktok, hvor man stort set på ingen måder kan se at det er et filter. Debatten går både ud på at det er en forfalsket udgave af ens ansigt, og at det kan bidrage til opfattelsen om at det er sådan man ser flottest ud når man filmer sig selv. Mit program er også inspireret af kortfilmen "Data Economy" af Trieuvy Luu and Martijn van den Broeck. Kortfilmen undersøger tanken om hvor langt mennesket er villig til at gå for, at tjene flere penge/eller få mere underholdning ved at sælge alt data.


Mit program skal forestille en app, hvor en (fiktiv) AI guider brugeren til at se pænere ud, ved at fortælle at man fx. skal se gladere ud, smile mere eller måske endda tage tøjet af. Når man tager billedet bliver det solgt med det samme, med en pris "AI'en" har sat. På den måde ville man kunne tjene flere og flere penge, afhængigt af hvor mange billeder man tog, og hvad "kvaliteten" af dem ville være. Dette er selvfølgelig ikke noget vi har, men det kan perspektiveres til hvordan det på de sociale medier hurtig kan blive en konkurrence om at se mest glad, lækker, og perfekt ud. Selvfølgelig er det mest noget der kan have en betydning hvis man er influencer der tjener penge på de sociale medier, når det kommer til relationen med økonomi. Her er det dog ikke data man sælger på de sociale medier i dette perspektiv, eller det i hvert fald ikke det der er intentionen, men det kan have en stor betydning hvor ofte man poster, hvor glad man ser ud, og især hvor lækker man ser ud, når det kommer til at tjene penge på at dele billeder. 


Når det kommer til data, er det muligvis ikke selve billederne "dataindsamlerne" er så interesseret i, da de sandsynligvis ikke er interesserede i hvordan vi ser ud. Tværtimod, er det hvordan vi agerer i hverdagen der er værd for dem at vide,  ift. reklametilpasning, statistik og andet. Hvis man som eksempel tager udgangspunkt i appen Tiktok, er data noget indehaverne af Tiktok i stor grad er interesseret i. De har for eksempel gjort så de kan opsamle en masse data fra masse andre ting  man foretager sig på sin telefon end at være på tiktok. (Schuldt, 2023). Det kan være svært at have en holdning til det, for teknisk set acceptere man denne dataindsamling hos fx Tiktok ved at downloade appen og godkende dette ved start, da det ofte er sjældent man læser brugerbetingelserne ordentligt npr man downloader apps. Personligt føler jeg, at jeg alligevel ikke har noget at skjule. Det eneste jeg ikke har tænkt over, er den ulige byttehandel der opstår ift. til den lille mængde underholdning man får, og den gigantiske mængde data de får af mig og mine hverdags oplevelser.


I dokumentaren " Shoshana Zuboff on Surveillance Capitalism", diskuterer Zuboff konceptet med "surveillance capitalisme"/overvågningskapitalisme og dets konsekvenser for samfundet. Zuboff argumenterer for at surveillance capitalism er en ny form for kapitalisme, der bygger op indsamling og analyse af perondata for at forudsige og påvirke menneskers adfærd. I dokumentaren forkklares om hvordan virksomheder som Google og Facebook, ligesom Tiktok, har bygget deres egne foretningsmodeller op omkring idéen med at indsamle kæmpe mængder date om deres brugere for at skabe mere målrettede reklamer og for at påvirke brugeradfærden. Her diskuterer Zuboff også hvilke konsekvenser surveillance capitalisms manipulation har for vores privatliv og demokrati, og argumenterer altså for at det udgør en trussel mod begge.  


Jeg synes det er kompliceret at danne overblik over disse problematikker, da det er problemer der ligger i et område inden for teknologi, der ikke er tilgængeligt for alle. Samtidig stoler jeg egentlig på at hvis vores cyber-secuerty hold i Danmark siger god for det, vil jeg selv fortsætte med at bruge disse platforme. I princippet ville det betyde, at jeg er villig til at betale for underholdningen med min data. Her kan man så rejse et helt andet spørgsmål om, om jeg så er med til at bidrage til at gøre en app som Tiktoks indehavere mere magtfulde?


Littaratur:

Schuldt, 2023, https://nyheder.tv2.dk/tech/2023-02-24-nu-forbydes-tiktok-flere-steder-og-det-er-der-god-grund-til-siger-eksperter

Trieuvy Luu and Martijn van den Broeck, Data Economy, 2017, https://vimeo.com/197395760
