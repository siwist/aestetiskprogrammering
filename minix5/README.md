# minix5
Her er et link til mit program: [Scrabble](https://stetisk-programmering.gitlab.io/aestetiskprogrammering/minix5/index.html)


Her er koden: [Koden til scrabble](https://gitlab.com/stetisk-programmering/aestetiskprogrammering/-/blob/main/minix5/minix5.js)


Her er et par screenshots:


<img src="Skærmbillede_2023-03-15_194118.png">
<img src="Skærmbillede_2023-03-15_194212.png">

# Beskrivelse af programmet
Mit program skal forestille et scrabble spil, hvor der bliver lagt tilfægige brikker på brættet. Brikkerne starte med at blive lagt nede i venstre hjøre. Når den har lagt brikker tilfældigt over hele brættet, dsv når den når op i øverste venstre hjørne, starter den efter lidt vente tid med at ligge brikker igen nede fra venstre hjørne, men denne gang tilfældigt ude for felterne, så det kommer til at se rodet ud. Denne funktion fortsætter igen og igen, og brættet vil derfor blive mere og mere fyldt, med scrabblebrikker.

# Beskrivelse af koden
I min kode startede jeg med at lave en masse globale variabler, såsom:
~~~~
let y = 0;
minAfstand = 75;
minWeight = 1;
let r;
~~~~
Jeg lavede også et array til at generere tilfædige bogstaver:
~~~~
let bogstav = ["A","B","C","D","E","F","G" ..........]
~~~~
Derefter satte jeg mit canvas, min baggrund og min framerate, samt definerede to funktioner der skulle tegne mit gitter. Jeg satte også min funktion drawGitter(); ind her, da den ikke skal inddrages i "draw-loopet", men bare være på canvas.
~~~~
 gitterWidth = 750/10;
 gitterHeight = 750/10;
 drawGitter();
~~~~
Jeg tegnede så min funktion "function drawGitter()", ved at lave et forloop med et forloop inden i. På den måde fik jeg to firkanter til at generere sig selv over mit canvas. I yderste forloop definerede jeg i til at være 0, sagde den skulle være mindre en bredte af canvas, og at i skuller plusses med gitterWidth hver gang. Med forloopet inden i, gjrode jeg det samme, men med en ny konstant "j", og at den skulle plusset med gitterHeight. Jeg satte to rects med i og j som placering, og gitterWidth, og gitterHeight som størrelse. Jeg kunne egentlig godt have lavet en enkel rect, med begge størrelse som størrelse, da de er defineret ens. Grundet til jeg ikke har gjordt dette er fordi jeg tænke det skulle passe til viduet ved ændring, men her blev firkanterne ikke kvadratiske. Functionen kom til at så sådan ud:


~~~~ 
function drawGitter(){

    push();
    for(i = 0 ; i < width; i+=gitterWidth){
        for(j = 0 ; j < height; j += gitterHeight){
            noFill();
            stroke(150);
            strokeWeight(1);
            rect(i,j,gitterWidth);
            rect(i,j,gitterHeight);
             
              }
    }
    pop();
}
~~~~


Herefter begyndte jeg min draw-function. Her definrede jeg rød, grøn og blå til at komme med et random tal, så de tilsammen kunne generere en farve. Jeg satte også en const, som konstant ville generere random tal op til 1.


~~~~
    r = random(255);
    g = random(255);
    b = random(255); 

    const minRandom = random(1);
~~~~


Som jeg har skrevet i min kode, ændrede jeg også origin, og sagde at x3 skulle minuses med minAfstand jeg definerede tidligere.
~~~~
    
    push();
    //translater for nemmere at kunne få brikerne til at starte i nedre højre hjørne
    translate(750,750);

    //får x3 til af flytte mod venstre
    x3 -= minAfstand;
~~~~


Den sidste del af min kode består af nogle if-statements. Den første er et if-statement hvor loopet går i gang hvis const genere et random tal der er mindre end 0.5. Her bliver der tegnet en scrabblebrik, med et tilføldigt bogstav fra det array jeg lavede i starten, samt et tilfældigt tal mellem 1 til 10. Som jeg har skrevet har jeg brugt floor inden mine random funktioner for at få det tilfældige tal rundet ned til et helt, så der fx ikke står 4.89548973 på en scrabblebrik.
~~~~
        if(minRandom < 0.5){
            //selve brikken
            stroke(0);
            fill(255,255,230);
            rect(x3+3,y3-70, floor(random(65,70)));
            
            //bogstaver og tal
            fill(r,b,g);
            textSize(50);
            text(random(bogstav),x3+20,y3-18);
            textSize(10);
            //floor runder ned til"gulvet"
            text(floor(random(1,10)),x3+60,y3-59);
~~~~
    

Efter det første if-stament, har jeg et else-statement hvor der ikke står noget i, for at skabe tilfældig plads mellem brikkerne. Dette vil svarer til et if-statement der sker hvis const er større end 0.5.

        } else{
            // ingen ting for at skabe lidt variation
        } 
        pop();
    


Nedestående if-statement er, som jeg har skrevet, til for at rykke brikkernes y-placering opad hver gang x er nået -width/0 af canvas.
~~~~
       
        push();
        //får "linjen" rykket opad
        if (x3 < -750){
            y3 = y3 - minAfstand;
            x3 = 0;
        }
~~~~


Det sidste if-statement, for scrabblebrikkerne til at lægge sig mere tilfældigt efter at y-placeringen af brikkerne har nået toppen, og får brættet til at se rodet ud. Jeg startede med bare at ville have min sketch til at starte forfra, og brugte her //window.location.reload();. Jeg ombestemte mig til at lave det rodede i stedet, men har ikke lige fjernet denne funktion helt. Som jeg har skrevet har jeg sat y3 til 400. Dette har jeg gjordt så brikkerne faktisk starter uden for canvas, hvor man bare ikke kan se det. Det er en lille snydemåde for at der går lidt tid før at man kan se at brikkerne bliver placeret rodet.

~~~~
        
        //starter loopet med at sætte brikkerne random
        if (y3 < -750){
            //window.location.reload();
            //har sat y3 til 400, for at den starter ude for canvas
            //på den på tager det lidt tid før brikkerne ligges tilfædigt.
            y3 = 400;
            x3 = 0;
            minAfstand = random(100,250);
            

        }

         pop();
~~~~

# Reflektion
I mit program har jeg programmeret tre regler. Den første regel skal genereres et gitter i mit setup. Anden regel sørger for, at der hele tiden genereres brikker på linjer fra højre til vestre, opad. Den sidste regel er en regel der gør, at når hele brættet er fyldt ud, skal der genereres brikker med tilfældig afstand ift. x- og y-placering. Disse regler spiller den rolle, at de kan generer tilfældige vrøvle ord. En gang imellem kommer der til at stå ord som "fy", "pip", og andre små ord man kan genkende. Dette er med til at skabe et unikt kunstværk, og er også med til at skabe en sjov vinkel på, om man kan spille scrabble med en computer. Muligvis kan man, men mon den kan have samme humor ift. At skrive sjove ord på trods af færre point, som vi mennesker kan finde på?


Dette er noget der kan relateres til begrebet "randomness" ift. Programmering og digital kultur, som blandt andet Nick Monfort et al. skriver om i et kapitel i bogen  "10 PRINT CHR$(205.5+RND(1)); : GOTO10". I kapitlet udforskes tilfældighedens rolle både historisk, kunstnerisk og digitalt. For eksempel findes "randomness" meget i kunsten, og især i kunstarten dadaismen. Denne kunstform er en der trådte væk fra normen, der benyttede sig meget af tilfældighed og "vrøvl", hvilket jeg synes mit program kan bære et præg af også. I kapitlet undersøges også de filosofiske implikationer af "randomness" i digital kultur, herunder dens forhold til determinisme og fri vilje. Her argumenteres for, at tilfældighed kan anses som en form for modstand mod den deterministiske del af programmering og digitale kultur, hvor tilfældigheden formår at introducere uforudsigelighed og chance i disse systemer. 


Begrebet "randomness" og dets relation til kunst, har fået mig til at tænke over hvad der er kunsten i et generativt program, når værket ikke  nødvendigvis er ens hver gang. Der er nemlig mange forskellige definitioner på hvad kunst er. Det at en handling også kan være kunst var fx noget af det første jeg tænkte på dette program passede bedst i, men det er jo ikke handlingen jeg selv har lavet, det er computeren der står for dette, ift at generere en tilfældighed. Dette gør det lidt svært at omfavne dette som kunst med det samme man møder et generativt kunstværk. På baggrund af det jeg har læst, og hvad vi også har haft om i software studier, kan det dog være selve intentionen og hvordan det kan bedømmes, der definerer dette. Jeg læste dette i en tekst af Philip Galanter, der i sin tekst diskutere og reflektere over hvad generativt kunst er. Galanter skriver for ekspemel:


"Are generative systems creative? What is required to create a truly creative computer? Most people would agree that digital generative art systems, and generative art systems in general, do not have ideas in a sense that implies consciousness. However, successful generative art systems commonly create new and surprising artifacts. The question of value is yet something else. As discussed in the context of the fitness bottleneck, the assignment of value to the results of generative art systems for the most part requires human judgment." (Philip Galanter, 2016)


Især sætningen "successful generative art systems commonly create new and surprising artifacts.", pointere meget godt hvordan det at bruge regler til at skabe et kunstværk, er kunsten i sig selv. Man kan også drage perspektiver til kunst som en handling. Man kan finde eksempler på dette i modebranchen, hvor er fashionshow er til for at vise designerens kunstværk - tøjet. I forskellige modeshows, har det dog ikke været tøjet der har været i centrum af kunsten, men handlingen hele modeshowet udspilles i. Et eksempel på dette kan være hvordan den sveske designer Beate Karlsson instruerede et modeshow, hvor det tøj hun havde designet, faldt fra hinanden i mens modellerne gik. Selve kunsten i dette er netop "reglen", Beate Karlson satte, og ikke tøjet der faldt fra hinanden. Bag kunsten var der selvfølgelig også en kritisk vinkel, der gik ud på at sætte fokus på at stoppe fast-fashion.



Litteratur:

• Anjali Takur, 2023, https://www.ndtv.com/world-news/at-milan-fashion-week-models-had-clothes-falling-apart-on-the-runway-heres-why-3836967
•  Philip Galanter, “Generative Art Theory,” in Christiane Paul, ed., A Companion to Digital Art (Oxford: Blackwell, 2016, p. 172
• Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, (Cambridge, MA: MIT Press, 2012), 119-146. [find PDF her https://10print.org/]
