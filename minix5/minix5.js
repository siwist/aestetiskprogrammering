//globale variabler
let x = 0;
let y = 0;
minAfstand = 75;
minWeight = 1;
let r;
let g;
let b;
let a;
let x2 = 0;
let y2 = 0;
let x3 = 0;
let y3 = 0;
let bogstav = ["A","B","C","D","E","F","G","H"," I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","Æ","Ø","Å"];
let gitterWidth; 
let gitterHeight;

function setup(){
    createCanvas(750,750);
    frameRate(5);
    //gitterWidth og Height defineret som bredten på kvadraterne
    gitterWidth = 750/10;
    gitterHeight = 750/10;
    background(255,255,245);
    //kaldt funktion nedenfor
    drawGitter();

}

function drawGitter(){

    push();
    for(i = 0 ; i < width; i+=gitterWidth){
        for(j = 0 ; j < height; j += gitterHeight){
            noFill();
            stroke(150);
            strokeWeight(1);
            rect(i,j,gitterWidth);
            rect(i,j,gitterHeight);
             
              }
    }
    
    pop();

}


function draw(){
    
    r = random(255);
    g = random(255);
    b = random(255); 

    const minRandom = random(1);
    
    push();
    //translater for nemmere at kunne få brikerne til at starte i nedre højre hjørne
    translate(750,750);

    //får x3 til af flytte mod venstre
    x3 -= minAfstand;
   
        if(minRandom < 0.5){
            //selve brikken
            stroke(0);
            fill(255,255,230);
            rect(x3+3,y3-70, floor(random(65,70)));
            
            //bogstaver og tal
            fill(r,b,g);
            textSize(50);
            text(random(bogstav),x3+20,y3-18);
            textSize(10);
            //floor runder ned til"gulvet"
            text(floor(random(1,10)),x3+60,y3-59);

        } else{
            // ingen ting for at skabe lidt variation
        } 
        pop();
       
        push();
        //får "linjen" rykket opad
        if (x3 < -750){
            y3 = y3 - minAfstand;
            x3 = 0;
        }
        
        //starter loopet med at sætte brikkerne random
        if (y3 < -750){
            //window.location.reload();
            //har sat y3 til 400, for at den starter ude for canvas
            //på den på tager det lidt tid før brikkerne ligges tilfædigt.
            y3 = 400;
            x3 = 0;
            minAfstand = random(100,250);
            

        }

         pop();
      

}
   